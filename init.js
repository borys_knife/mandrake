let b1 = document.querySelector('.b1')
let b2 = document.querySelector('.b2')
let b3 = document.querySelector('.b3')

let changeButton = function (color, text) {
  this.style.background = color
  this.textContent = text
}

let bound1 = changeButton.bind(b1, 'red', 'clicked on 1')
let bound2 = changeButton.bind(b2, 'blue', 'clicked on 2')
let bound3 = changeButton.bind(b3, 'green', 'clicked on 3')
b1.onclick = () => {
  setTimeout(bound1, 1000)
}
b2.onclick = bound2
b3.onclick = bound3

let os = ['Linux', 'Windows', 'Macintosh', 'Android']
if (os.length < 5) {
  os.push('Symbian')
} else {
  alert('Массив превышает допустимую длину')
}
console.log(os)

let win = os.find((os) => os === 'Linux')
if (win === undefined) {
  os.push('BadaOs')
}

let button4 = document.querySelector('.b4')
let mainBlock = document.getElementById('mainblockTextcontent')
let changeBlock = function (textcontentMain, backgroundMain, fontsizeMain) {
  this.textContent = textcontentMain
  this.style.background = backgroundMain
  this.style.fontSize = fontsizeMain
}
let chF = changeBlock.bind(mainBlock, 'Главный блок изменен', '2rem', 'white')
button4.onclick = chF
